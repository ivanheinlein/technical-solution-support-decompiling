enum ShapeStyles {
    STROKE_3px(1),
    NOTHING(3),
    STROKE_7px(4),
    GRADIENT(7),
    COLOR_RED(8);

    private int value;

    ShapeStyles(int value){
        this.value = value;
    }
    public Integer getValue(){ return value; }
}
