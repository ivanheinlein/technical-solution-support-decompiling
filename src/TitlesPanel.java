/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import javax.swing.JPanel;
import javax.swing.Timer;

public class TitlesPanel
extends JPanel
implements ActionListener {
    private Graphics2D g2d;
    private Timer animation;
    private boolean is_done = true;
    private int start_angle = 0;
    private ShapeFactory shape;

    /**
     * <p>TitlesPanel constructor</p>
     * @param _shape ShapeFactory
     */
    public TitlesPanel(ShapeFactory _shape) {
        this.shape = _shape;
        this.animation = new Timer(50, this);
        this.animation.setInitialDelay(50);
        this.animation.start();
    }

    @Override
    /**
     * <p>Overrided interface method {@link ActionListener}</p>
     * <p>do repaint when all calculation done</p>
     * @return void
     */
    public void actionPerformed(ActionEvent arg0) {
        if (this.is_done) {
            this.repaint();
        }
    }

    private void doDrawing(Graphics g) {
        this.is_done = false;
        this.g2d = (Graphics2D)g;
        this.g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Dimension size = this.getSize();
        Insets insets = this.getInsets();
        int w = size.width - insets.left - insets.right;
        int h = size.height - insets.top - insets.bottom;
        this.g2d.setStroke(this.shape.stroke);
        this.g2d.setPaint(this.shape.paint);
        double angle = this.start_angle++;
        if (this.start_angle > 360) {
            this.start_angle = 0;
        }
        double dr = 90.0 / ((double)w / ((double)shape.width * 1.5));
        int j = this.shape.height;
        while (j < h) {
            int i = this.shape.width;
            while (i < w) {
                angle = angle > 360.0 ? 0.0 : angle + dr;
                AffineTransform transform = new AffineTransform();
                transform.translate(i, j);
                transform.rotate(Math.toRadians(angle));
                this.g2d.draw(transform.createTransformedShape(shape.shape));
                i = (int)((double)i + (double)this.shape.width * 1.5);
            }
            j = (int)((double)j + (double)this.shape.height * 1.5);
        }
        this.is_done = true;
    }

    @Override
    /**
     * <p>Overrided class method {@link javax.swing.JComponent}</p>
     * <p>call parent method and draw shape</p>
     * @param g Graphics
     * @return void
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.doDrawing(g);
    }
}

