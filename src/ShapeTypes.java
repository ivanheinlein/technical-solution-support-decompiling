enum ShapeTypes {
    HEXAGON(1),
    STAR(3),
    SQUARE(5),
    TRINGULAR(7),
    PAC_MAN(9);

    private int value;

    ShapeTypes(int value){
        this.value = value;
    }
    public Integer getValue(){ return value; }
}
